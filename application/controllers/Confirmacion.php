<?php

class Confirmacion extends CI_Controller {
    
    function __construct(){
        
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('date');
        $this->load->model('confirmaciones');
        $this->load->helper('spreadsheet');
        $this->load->helper('download');
    }
    
    public function index() {
         
        if ($this->session->userdata('isLoggedIn')){
            redirect('/Confirmacion/miembros');
        }else{
            redirect('/');
        }
        
    }
    
    function miembros(){
        $data = array( 'titulo' => "Reservas Heliópolis | Confirmación" );
        $this->load->view('Backend/header',$data);
        $this->load->view('confirmacion');
    }
    
    public function exportar_excel_resumen(){
        
        $sede = $this->session->userdata('sedeId'); 
 
        if ($sede == 999)  $sede = 0; 

        $listConfirmaciones = $this->confirmaciones->get_confirmaciones_sedes($sede);
        
        $header_row = $this->get_header_confirmaciones_resumen();
        $rows[] = $header_row;
        
        foreach($listConfirmaciones as $r) {

            $row = array();
            
            $row[] = $r['sede'];
            $row[] = $r['Confirmado'];
            $row[] = $r['NoConfirmado'];
            $row[] = $r['Pendiente'];
            $rows[] = $row;
        }
    
        $content = array_to_spreadsheet($rows);
	force_download('resumen_confirmaciones.xls', $content);
	//exit; 
        
    }
    
    public function exportar_excel(){
        
        $this->load->helper('spreadsheet');
        $this->load->helper('download');
        
        $sede = $this->session->userdata('sedeId'); 
        $ffvv = $this->session->userdata('ffvv'); 
        
        $search_sede = $this->session->userdata('search_sede');
        $search_ffvv = $this->session->userdata('search_ffvv'); 
        $search_asiste = $this->session->userdata('search_asiste');
        
        $page = $this->session->userdata('page');
        $limit = $this->session->userdata('limit');

        if ($sede == 999)  $sede = 0; 
        
        $start = $limit*$page - $limit; 
        $start = ($start<0)?0:$start; 
        
        $listConfirmaciones = $this->confirmaciones->getConfirmaciones($limit, $start,$sede,$ffvv,$search_sede, $search_ffvv, $search_asiste);
        
        $header_row = $this->get_header_confirmaciones();
        $rows[] = $header_row;
        
        foreach($listConfirmaciones as $r) {
            
            $row = array();
            
            $row[] = $r->id;
            $row[] = ucwords(strtolower($r->apellidos)).', '.ucwords(strtolower($r->nombre));
            $row[] = $r->sede;
            $row[] = $r->ffvv;
            $row[] = $this->get_confirmacion($r->asiste);
            $row[] = $this->get_date($r->fecha_llegada);
            $row[] = $this->get_horario($r->horario);
            $row[] = $r->observaciones;
            
            $rows[] = $row;
        }
    
        $content = array_to_spreadsheet($rows);
	force_download('Lista_confirmaciones.xls', $content);
	exit; 
        
    }
    
    function get_date($fecha_llegada){
        
        if($fecha_llegada){
            return nice_date($fecha_llegada, 'd/m/Y');
        }else{
            return "";
        }
    }
    
    function get_horario($horario){
        
        switch ($horario) {
            case 1:  return  "Desayuno";
            case 2:  return  "Almuerzo";
            case 3:  return  "Cena";
            default: return  "";
        }
    }
    
    function get_confirmacion($asiste){
    
        switch ($asiste) {
            case '':   return '';
            case 0: return 'No';
            case 1: return 'Si';
            default: return '';
        }
     
    }

    function get_header_confirmaciones_resumen(){
        
        $header_row = array();
	
	$header_row[] = 'Sede';
        $header_row[] = '# Confirmados';
        $header_row[] = '# No Asiste';
        $header_row[] = '# No Confirmados';

        return $header_row;
        
    }
    
    function get_header_confirmaciones(){
        
        $header_row = array();
	
	$header_row[] = 'Id';
        $header_row[] = 'Miembro';
        $header_row[] = 'Sede';
        $header_row[] = 'FFVV';
        $header_row[] = 'Asiste';
        $header_row[] = 'Fecha de llegada';
        $header_row[] = 'Horario';
        $header_row[] = 'Observaciones';
        
        return $header_row;
        
    }
    
    public function lista_miembros(){
        
        $sede = $this->session->userdata('sedeId'); 
        $ffvv = $this->session->userdata('ffvv'); 
        
        $search_sede = $this->input->post('sede');
        $search_ffvv = $this->input->post('ffvv'); 
        $search_asiste = $this->input->post('asiste');
        
        $page = $this->input->post('page');
        $limit = $this->input->post('rows');
        $sidx = $this->input->post('sidx');   
        $sord = $this->input->post('sord');
        
        if ($sede == 999)  $sede = 0; 
        
        $start = $limit*$page - $limit; 
        $start = ($start<0)?0:$start; 
        
        $data = array(
                   'search_sede'  => $search_sede,
                   'search_ffvv'  => $search_ffvv,
                   'search_asiste' => $search_asiste,
                   'limit' => $limit,
                   'start' => $start
            );

        $this->session->set_userdata($data);
        
        $countConfirmaciones = $this->confirmaciones->getCountConfirmaciones($sede,$ffvv,$search_sede, $search_ffvv, $search_asiste);
        $listConfirmaciones = $this->confirmaciones->getConfirmaciones($limit, $start,$sede,$ffvv,$search_sede, $search_ffvv, $search_asiste);
         
        $count = $countConfirmaciones;
        
        if( $count > 0 ) $total_pages = ceil($count/$limit);    
        else $total_pages = 0;
        
        $response = new stdClass(); 
        $response->page = $page;
        $response->total = $total_pages;
        $response->records = "".$count."";
        
        $i=0;
        
        foreach($listConfirmaciones as $row) {
            $response->rows[$i]['id']=$row->id;
            $response->rows[$i]['cell']=array($row->id,ucwords(strtolower($row->apellidos)).', '.ucwords(strtolower($row->nombre)),$row->sede,$row->ffvv,$row->asiste,$row->fecha_llegada,$row->horario,$row->observaciones);
            $i++;
        }

        echo json_encode($response);
        
    }
    
    public function actualizar_confirmacion(){
        
        $id = $this->input->post('id');
        $asiste = $this->input->post('asiste');
        $horario = $this->input->post('horario');
        $observaciones = $this->input->post('observaciones');
        $fecha_llegada = $this->input->post('fecha_llegada');
        
        if (isset($fecha_llegada) && $fecha_llegada <> '' ){
        	$fecha_llegada = explode('/', $this->input->post('fecha_llegada'));
        	$fecha_llegada  = date('Y-m-d H:i:s', strtotime(implode('-', array_reverse($fecha_llegada))));
        }
        
        if ($horario == -1) $horario = NULL;  
        
        if ($asiste == -1 || $asiste == 0 ) {
            $asiste = NULL;
            $fecha_llegada = NULL;
            $horario = NULL; 
        }
        
        if ($asiste == 0 ) $asiste = 0;
 
        $confirmacionData=array(
            'asiste' => $asiste,
            'fecha_llegada' => $fecha_llegada,
            'horario' =>  $horario,
            'observaciones' => $observaciones,
            'fecha_actualizacion' => standard_date('DATE_ISO8601', time())
        );
        
        $confirmacion= $this->confirmaciones->updateconfirmacion($id,$confirmacionData);
        
    }
    
    
}
