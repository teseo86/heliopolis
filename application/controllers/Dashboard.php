<?php

class Dashboard extends CI_Controller {
    
    function __construct() {
      
        parent::__construct();
        $this->load->model('confirmaciones');
        $this->load->library('session');
        $this->load->helper('url');
    }
    
    public function index() {
        if ($this->session->userdata('isLoggedIn')){
            $data = array( 'titulo' => "Reservas Heliópolis | Dashboard" );
            $this->load->view('Backend/header',$data);
            $this->load->view('dashboard');
        }else{
            redirect('/');
        }
  }

    public function get_confirmaciones_sedes(){
    
        $sede = $this->session->userdata('sedeId'); 
       
        if ($sede == 999)  $sede = 0;

        $table = array();

        $table['cols'] = array(
          array('label' => 'Sede', 'type' => 'string'),
          array('label' => 'Confirmado', 'type' => 'number'),
          array('label' => 'No asiste', 'type' => 'number'),
          array('label' => 'No confirmado', 'type' => 'number')
        );

        $rows = array();

        $confirmaciones_sedes = $this->confirmaciones->get_confirmaciones_sedes($sede); 

        foreach($confirmaciones_sedes as $row) {
            $temp = array();
            $temp[] = array('v' => (string) $row['sede']); 

            // Values of column
            $temp[] = array('v' => (int) $row['Confirmado']); 
            $temp[] = array('v' => (int) $row['NoConfirmado']); 
            $temp[] = array('v' => (int) $row['Pendiente']); 
            $rows[] = array('c' => $temp);
        }

        $table['rows'] = $rows;

        echo json_encode($table);
  }

    
    
    public function get_confirmados_ffvv(){
    
        $sede = $this->session->userdata('sedeId'); 
       
        if ($sede == 999)  $sede = 0;

        $table = array();

        $table['cols'] = array(
          array('label' => 'FFVV', 'type' => 'string'),
          array('label' => 'Cantidad', 'type' => 'number'),
        );

        $rows = array();

        $confirmaciones_sedes = $this->confirmaciones->get_confirmados_ffvv($sede); 

        foreach($confirmaciones_sedes as $row) {
            $temp = array();
            $temp[] = array('v' => (string) $row['ffvv']); 
            $temp[] = array('v' => (int) $row['numMiembros']); 
            $rows[] = array('c' => $temp);
        }

        $table['rows'] = $rows;

        echo json_encode($table);
    
  }

    public function get_confirmados_horario_fecha(){
    
        $sede = $this->session->userdata('sedeId'); 
       
        if ($sede == 999)  $sede = 0;

        $table = array();

        $table['cols'] = array(
          array('label' => 'Fecha', 'type' => 'string'),
          array('label' => 'Desayuno', 'type' => 'number'),
          array('label' => 'Almuerzo', 'type' => 'number'),
          array('label' => 'Cena', 'type' => 'number'),
          array('label' => 'Vacio', 'type' => 'number'),
        );

        $rows = array();

        $confirmaciones_sedes = $this->confirmaciones->get_confirmados_horario_fecha($sede); 

        foreach($confirmaciones_sedes as $row) {
            
            $temp = array();
            $temp[] = array('v' => date("d/m/y", strtotime($row['fecha']))); 
            $temp[] = array('v' => (int) $row['Desayuno']); 
            $temp[] = array('v' => (int) $row['Almuerzo']); 
            $temp[] = array('v' => (int) $row['Cena']); 
            $temp[] = array('v' => (int) $row['Vacio']); 
            $rows[] = array('c' => $temp);
        }

        $table['rows'] = $rows;

        echo json_encode($table);
    
  }

}

?>