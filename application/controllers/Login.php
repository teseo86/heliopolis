<?php

class Login extends CI_Controller {

    function __construct(){
        
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('usuarios');
    }
  
    public function index() {
        
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
         
        $data = array(
            'titulo' => "Reservas Heliópolis | Login Usuario",
            'errormsg' => $this->session->flashdata('errormsg')
        );
        
        $this->load->view('Backend/header',$data);
        $this->load->view('login');
    }
    
    function check_database($password) {

        $user = $this->input->post('user');
        
        $result = $this->usuarios->login($user, $password);
        
        if ($result) {
            
            $data = array(
                   'user'  => $user,
                   'nombre'  => $result->encargado,
                   'sedeId' => $result->sede,
                   'permiso' => $result->nivel_acceso,
                   'ffvv' => $result->idffvv,
                   'isLoggedIn' => TRUE
            );

            $this->session->set_userdata($data);
            return true;
        } else {
            $this->form_validation->set_message('check_database', 'Usuario o password inválido');
            $this->session->set_flashdata('errormsg', 'Usuario o password inválido');
            return false;
        }
    }
    
    function checklogin(){

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('url');
        
        $this->form_validation->set_rules('user', 'User', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
        
        if ($this->form_validation->run() == FALSE) {
            redirect('/');
        } else {
            redirect('/Confirmacion');
        }
    }
    
    public function logout(){
        
        $this->session->sess_destroy();
        redirect('/');
    }
    
}
    

