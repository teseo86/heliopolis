<?php

class Sede extends CI_Controller {
	
    function __construct(){
        
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('sedes');
        $this->load->library('session');
    }
    
    public function index() {
         
        if ($this->session->userdata('isLoggedIn')){
            redirect('/Confirmacion/miembros');
        }else{
            redirect('/');
        }
        
    }
    
    public function lista_sedes() {
         
        $response = '<select>'; 
        $sede = $this->session->userdata('sedeId');
        
        if ($sede == 999) { 
            $response .='<option value="0">Todos</option>' ; 
            $sede = 0;
        }
        
        $listSedes = $this->sedes->getSedes($sede); 
        
    	foreach($listSedes as $row) {              
            $response .= '<option value="'.$row['id'].'">'.$row['sede'].'</option>';     
        }
    	
    	$response .= '</select>';

    	echo $response;
        
    }

}
