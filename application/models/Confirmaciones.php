<?php

class Confirmaciones extends CI_Model {
    
    public function getConfirmaciones($limit, $start, $idSede = 0, $ffvv = 0, $search_sede=0, $search_ffvv=0, $search_asiste=-2 ) {

        $this->db->select('mcc.id , s.s_nombsede as sede , fv.s_desc_abrev as ffvv, IFNULL(asiste,"") asiste, fecha_llegada, IFNULL(horario,"") horario , IFNULL(observaciones,"") observaciones, case when length(ifnull(s_nombabrev, "")) = 0 then s_nomb1 else s_nombabrev end nombre , CONCAT( mpn.s_apepat,  " " , IFNULL( mpn.s_apemat,  " " ) ) as apellidos ');

        $this->db->from('mov_confirmaciones mcc');
        $this->db->join('mae_personanatural mpn', 'mcc.idpersona = mpn.n_idpersona');
        $this->db->join('mae_rolsede s', 's.n_idsede = mcc.idsede');
        $this->db->join('mae_cursosnacionales c', 'c.n_idcursonacional = mcc.idcurso and c.s_estado = "A" ');
        $this->db->join('tab_dominio_desc fv', 'n_iddominio = 13 and fv.n_idarg = mcc.idffvv');
        
        $this->db->where('convocado', 1);
        
        if ($idSede > 0)  $this->db->where('idsede', $idSede);
        if ($ffvv > 0)  $this->db->where('idffvv', $ffvv); 
        if ($search_ffvv> 0)  $this->db->where('idffvv', $search_ffvv);
        if ($search_sede> 0)  $this->db->where('idsede', $search_sede);
        
        if ($search_asiste > -1)  $this->db->where('asiste', $search_asiste); 
        elseif ($search_asiste == -1) $this->db->where(array('asiste' => NULL));
        	
        $this->db->order_by("s.n_idsede", "asc"); 
        $this->db->order_by("fv.s_desc_abrev", "asc");    
        $this->db->order_by("apellidos", "asc");
        
        $this->db->limit($limit,$start);

        $query = $this->db->get();   
        
        return $query->result();
    }
    
    public function getCountConfirmaciones($idSede = 0, $ffvv = 0, $search_sede=0, $search_ffvv=0, $search_asiste=-2 ) {

        $this->db->select('mcc.id , s.s_nombsede as sede , fv.s_desc_abrev as ffvv, IFNULL(asiste,"") asiste, fecha_llegada, IFNULL(horario,"") horario , IFNULL(observaciones,"") observaciones, case when length(ifnull(s_nombabrev, "")) = 0 then s_nomb1 else s_nombabrev end nombre , CONCAT( mpn.s_apepat,  " " , IFNULL( mpn.s_apemat,  " " ) ) as apellidos ');

        $this->db->from('mov_confirmaciones mcc');
        $this->db->join('mae_personanatural mpn', 'mcc.idpersona = mpn.n_idpersona');
        $this->db->join('mae_rolsede s', 's.n_idsede = mcc.idsede');
        $this->db->join('mae_cursosnacionales c', 'c.n_idcursonacional = mcc.idcurso and c.s_estado = "A" ');
        $this->db->join('tab_dominio_desc fv', 'n_iddominio = 13 and fv.n_idarg = mcc.idffvv');
        
        $this->db->where('convocado', 1);
        
        if ($idSede > 0)  $this->db->where('idsede', $idSede);
        if ($ffvv > 0)  $this->db->where('idffvv', $ffvv); 
        if ($search_ffvv> 0)  $this->db->where('idffvv', $search_ffvv);
        if ($search_sede> 0)  $this->db->where('idsede', $search_sede);
        
        if ($search_asiste > -1)  $this->db->where('asiste', $search_asiste); 
        elseif ($search_asiste == -1) $this->db->where(array('asiste' => NULL));

        $query = $this->db->count_all_results();   
        
        return $query;
    }
    
    public function updateconfirmacion($id,$data){
        
        $this->db->where('id', $id);
        $this->db->update('mov_confirmaciones', $data); 
    }

    function get_confirmaciones_sedes($sede){

        $this->db->select('s.s_nombsede as sede, SUM( CASE WHEN asiste =1 THEN 1 ELSE 0 END ) Confirmado, SUM( CASE WHEN asiste =0 THEN 1 ELSE 0 END ) NoConfirmado, SUM( CASE WHEN asiste IS NULL THEN 1 ELSE 0 END ) Pendiente ');
        $this->db->from('mov_confirmaciones mcc');
        $this->db->join('mae_rolsede s', 's.n_idsede = mcc.idsede');
        $this->db->join('mae_cursosnacionales c', 'c.n_idcursonacional = mcc.idcurso and c.s_estado = "A" ');
        $this->db->where('convocado', 1);

        if ($sede>0)  $this->db->where('idsede', $sede);

        $this->db->group_by('idsede');
        $this->db->order_by('sede');

        $query = $this->db->get(); 
        return $query->result_array();

    }

    function get_confirmados_ffvv($sede){

        $this->db->select('fv.s_desc_abrev as ffvv, count(*) as numMiembros');
        $this->db->from('mov_confirmaciones mcc');
        $this->db->join('tab_dominio_desc fv', 'n_iddominio = 13 and fv.n_idarg = mcc.idffvv');
        $this->db->join('mae_cursosnacionales c', 'c.n_idcursonacional = mcc.idcurso and c.s_estado = "A" ');
        $this->db->where('convocado', 1);
        $this->db->where('asiste', 1);

        if ($sede>0)  $this->db->where('idsede', $sede);

        $this->db->group_by('idffvv');

        $query = $this->db->get(); 
        return $query->result_array();

    }

    function get_confirmados_horario_fecha($sede){

        $this->db->select('fecha_llegada as fecha, SUM( CASE WHEN horario =1 THEN 1 ELSE 0 END ) Desayuno, SUM( CASE WHEN horario =2 THEN 1 ELSE 0 END ) Almuerzo, SUM( CASE WHEN horario =3 THEN 1 ELSE 0 END ) Cena, SUM( CASE WHEN horario IS NULL THEN 1 ELSE 0 END ) Vacio');
        $this->db->from('mov_confirmaciones mcc');
        $this->db->join('mae_cursosnacionales c', 'c.n_idcursonacional = mcc.idcurso and c.s_estado = "A" ');
        $this->db->where('convocado', 1);
        $this->db->where('asiste', 1);

        if ($sede>0)  $this->db->where('idsede', $sede);

        $this->db->group_by('fecha_llegada');

        $query = $this->db->get(); 
        return $query->result_array();
    }


    
}

