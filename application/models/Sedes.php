<?php

class Sedes extends CI_Model {

    public function getSedes($sede = 0) {
        
        $this->db->select('n_idsede as id, s_nombsede as sede');
        $this->db->from('mae_rolsede');
        $this->db->where('n_tipo_status_sede',1);
        
        if ($sede>0) $this->db->where('n_idsede',$sede);
        
        $query = $this->db->get();
        
        return $query->result_array();
        
    }

}

