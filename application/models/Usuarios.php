<?php

class Usuarios extends CI_Model {

    function login($user, $password){
        
        $this->db->select('id, encargado, sede, an.nivel_acceso as nivel_acceso, idffvv');
        $this->db->from('administ_niveles_sistema ans');
        $this->db->join('administ a', 'a.id = ans.administ_id');
        $this->db->join('administ_niveles an', 'an.id_nivel = ans.administ_niveles_id_nivel');
        $this->db->where('us_uario', $user);
        $this->db->where('pas_sclave', MD5($password));
        $this->db->where('ans.sistemas_id_sistema', 3);
        $this->db->limit(1);
 
        $query = $this->db->get();
 
        if($query->num_rows() == 1){
          return $query->row();
        }else{
          return false;
        }
    }

}
