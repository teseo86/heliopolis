<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="shortcut icon" href="<?= base_url()?>template/ico/favicon.png">
        <title><?= $titulo; ?> </title>
        <!-- Bootstrap core CSS -->
        <link href="<?= base_url() ?>template/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet" />
        <link href="<?= base_url()?>template/jqgrid/css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?= base_url()?>template/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" media="screen" />
        <!-- Custom styles for this template -->
        <link href="<?= base_url()?>template/css/style.css" rel="stylesheet">

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
        <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
        <script src="<?= base_url()?>template/bootstrap/js/bootstrap.min.js"></script> 
        <!-- Placed at the end of the document so the pages load faster --> 

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?= base_url()?>template/jqgrid/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <script src="<?= base_url()?>template/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
    </head>
<body>
    <div id="wrapper">
        <div class="header">
            <nav class="navbar navbar-site navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> 
                            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> 
                            <span class="icon-bar"></span> 
                        </button>
                        <a href="index.html" class="navbar-brand logo logo-title"> 
                            <!-- Original Logo will be placed here  --> 
                            <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> Sistemas de Reservas
                            <span>Heliopolis </span> 
                        </a> 
                    </div>
                    <div class="navbar-collapse collapse">
                        <?php if ($this->session->userdata('isLoggedIn')){ ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?php echo base_url('Login/logout'); ?>">Cerrar Sesión <i class="glyphicon glyphicon-off"></i> </a></li>
                            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span><?= $this->session->userdata('nombre')?></span> <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>

                            </li>   
                        </ul>
                        <?php     
                        }
                        ?> 
                        
                    </div>
                    <!--/.nav-collapse --> 
                </div>
                <!-- /.container-fluid --> 
            </nav>
        </div>
        <!-- /.header -->
        