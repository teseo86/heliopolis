  <script type="text/javascript">
            $(document).ready(function(){
            $("#tblconfirmaciones").jqGrid({
                    url:'<?php echo base_url("confirmacion/lista_miembros" );?>',
                    editurl: '<?php echo base_url("confirmacion/actualizar_confirmacion" );?>',
                    datatype: 'json',
                    mtype: 'post',
                    colNames:['Id','Nombre', 'Sede','FFVV','Asiste', 'Fecha Llegada','Horario','Observación'],
                    colModel:[
                        {name:'id', index:'id', width:25, search: false, resizable:false, align:"center"},
                        {name:'miembro', index:'miembro', width:150,resizable:false, search: false, sortable:true},
                        {name:'sede', index:'sede', width:90,resizable:false, sortable:true, stype:'select', search:true, surl:'<?php echo base_url("sede/lista_sedes" );?>'},
                        {name:'ffvv', index:'ffvv', width:40,resizable:false, sortable:true, search: true, stype:'select', searchoptions: {  value: "0:Todos;1:CDS;2:BBFF;3:BBMM", defaultValue: "0" } },
                        {name:'asiste', index:'asiste', editable: true, search: true, width:45, formatter:'select', editrules:{ required:true}, edittype:"select", editoptions: { value:"-1:Seleccione;0:No;1:Si"}, stype:'select', searchoptions: {  value: "-2:Todo;-1:Sin confirmar;1:Si;0:No", defaultValue: "-2" }  },
                        {name:'fecha_llegada', index:'fecha_llegada', search: false,editable: true, formatter:'date',  srcformat:"d/mm/Y", newformat:"d/mm/Y", width:100, editoptions: {
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                               $(element).datepicker({
                                    autoclose: true,
                                    dateFormat: 'd/mm/y',
                                    orientation : 'auto bottom',
                                    showOn: 'focus'
                                });
                            }
                        }},
                        {name:'horario', index:'horario', search: false,editable: true,  width: 70 , formatter:'select', edittype:"select",  editoptions: { value: "-1:Seleccione; 1:Desayuno; 2:Almuerzo;3:Cena" } },
                        {name:'observaciones', index:'observaciones', search: false, editable: true, edittype: "text", width:190}
                    ],
                    pager: '#paginacion',
                    rowNum:100,
                    rowList:[100,250,500,1000],
                    sortname: 'id',
                    sortorder: 'asc',
                    viewrecords: true,
                    height:'auto',
                    rownumbers: true,
                    gridview: true,
                    onSelectRow: editRow,
                    caption: 'Confirmaciones'       
                });    
                
                $('#tblconfirmaciones').jqGrid('filterToolbar', { searchOnEnter: true, enableClear: false });
                
                var lastSelection,paginador ;
                
                paginador = $("#tblconfirmaciones").getGridParam('pager');
                $('#tblconfirmaciones').navGrid(paginador, {edit: false,add: false,del: false,search: false,refresh: false}).navButtonAdd(paginador, {
                    caption: "Exportar Excel",
                    buttonicon: "ui-icon-export",
                    onClickButton: function() {
                        $("#tblconfirmaciones").jqGrid('excelExport', { url: '<?php echo base_url("confirmacion/exportar_excel" );?>' }); 
                    },
                    position: "last"
            	});
                
                var lastSelection ;
                
                function editRow(id) {
                         
                         if (id ) {
                             var grid = $("#tblconfirmaciones");
                             grid.jqGrid('restoreRow', lastSelection);
                             grid.jqGrid('editRow',id,true);
                             lastSelection = id;
                         }
                }

            });
        </script>

        <div class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 page-sidebar">
                        <aside>
                            <div class="inner-box">
                                <div class="user-panel-sidebar">


                                    <!-- /.collapse-box  -->
                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Dashboard <a class="pull-right" data-toggle="collapse"  href="#MyAds"><i class="fa fa-angle-down"></i></a></h5>
                                        <div id="MyAds" class="panel-collapse collapse in">
                                            <ul class="acc-list">
                                                <li"><a href="<?php echo base_url('Dashboard'); ?>"><i class="icon-docs"></i> Dashboard </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- /.collapse-box  -->
                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Asistencia <a class="pull-right" data-toggle="collapse"  href="#MyAds"><i class="fa fa-angle-down"></i></a></h5>
                                        <div id="MyAds" class="panel-collapse collapse in">
                                            <ul class="acc-list">
                                                <li class="active"><a href="<?php echo base_url('Confirmacion'); ?>"><i class="icon-docs"></i> Confirmar asistencia </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- /.collapse-box  -->
                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Cerrar sesión <a class="pull-right" data-toggle="collapse"  href="#TerminateAccount"><i class="fa fa-angle-down"></i></a></h5>
                                        <div id="TerminateAccount" class="panel-collapse collapse in">
                                            <ul class="acc-list">
                                                <li><a href="<?php echo base_url('Login/logout'); ?>"><i class="icon-cancel-circled "></i> Cerrar sesión </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.collapse-box  --> 
                                </div>
                            </div>
                            <!-- /.inner-box  --> 

                        </aside>
                    </div>
                    <!--/.page-sidebar-->

                    <div class="col-sm-9 page-content">
                        <div class="inner-box">
                            <h2 class="title-2"><i class="icon-docs"></i> Asistencia Curso FFVV 2016 </h2>
                            <div class="table-responsive">

                                <table id="tblconfirmaciones"> </table>
                                <div id="paginacion"> </div>


                            </div>
                            <!--/.row-box End--> 

                        </div>
                    </div>
                    <!--/.page-content--> 
                </div>
                <!--/.row--> 
            </div>
            <!--/.container--> 
        </div>
        <!-- /.main-container -->


