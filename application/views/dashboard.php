

<div class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 page-sidebar">
                        <aside>
                            <div class="inner-box">
                                <div class="user-panel-sidebar">

                                    <!-- /.collapse-box  -->
                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Dashboard <a class="pull-right" data-toggle="collapse"  href="#MyAds"><i class="fa fa-angle-down"></i></a></h5>
                                        <div id="MyAds" class="panel-collapse collapse in">
                                            <ul class="acc-list">
                                                <li  class="active"><a href="<?php echo base_url('Dashboard'); ?>"><i class="icon-docs"></i> Dashboard </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- /.collapse-box  -->
                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Asistencia <a class="pull-right" data-toggle="collapse"  href="#MyAds"><i class="fa fa-angle-down"></i></a></h5>
                                        <div id="MyAds" class="panel-collapse collapse in">
                                            <ul class="acc-list">
                                                <li><a href="<?php echo base_url('Confirmacion'); ?>"><i class="icon-docs"></i> Confirmar asistencia </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- /.collapse-box  -->
                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Cerrar sesión <a class="pull-right" data-toggle="collapse"  href="#TerminateAccount"><i class="fa fa-angle-down"></i></a></h5>
                                        <div id="TerminateAccount" class="panel-collapse collapse in">
                                            <ul class="acc-list">
                                                <li><a href="<?php echo base_url('Login/logout'); ?>"><i class="icon-cancel-circled "></i> Cerrar sesión </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.collapse-box  --> 
                                </div>
                            </div>
                            <!-- /.inner-box  --> 

                        </aside>
                    </div>
                    <!--/.page-sidebar-->

                    <div class="col-md-9 page-content">
                        <div class="inner-box">
                            <h2 class="title-2"><i class="icon-docs"></i> Bienvenido al sistema de reservas de Cursos </h2>
                            <div>
                                <button id="exportarTablaResumen"> Exportar a excel</button>
                                <br>
                            </div>
                            <div id="table_confirmaciones_sedes"></div>
                            <div id="chart_confirmaciones_sedes"></div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div id="chart_confirmaciones_ffvv"></div>
                                </div>
                                <div class="col-md-7">
                                    <div id="chart_confirmaciones_fecha"></div>
                                </div>
                            </div>                            

                            <!--/.row-box End--> 

                        </div>
                    </div>
                    <!--/.page-content--> 
                </div>
                <!--/.row--> 
            </div>
            <!--/.container--> 
        </div>
        <!-- /.main-container -->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#exportarTablaResumen').click(function() { 
            $.ajax({
                url: '<?php echo base_url("confirmacion/exportar_excel_resumen" );?>',
                success: function(response){
                    window.open('<?php echo base_url("confirmacion/exportar_excel_resumen" );?>','_blank');
            },
            });   
        });
    });
    
    google.charts.load("current", {packages:['corechart','table']});

    google.charts.setOnLoadCallback(drawtableConfirmacionesSedes);
    google.charts.setOnLoadCallback(drawChartConfirmacionesSedes);
    google.charts.setOnLoadCallback(drawChartConfirmadosFFVV);
    google.charts.setOnLoadCallback(drawChartConfirmadosHorarioFecha);
      
    function drawtableConfirmacionesSedes() {
      var jsonData = $.ajax({
          url: '<?php echo base_url("dashboard/get_confirmaciones_sedes" );?>',
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      var options = {
            showRowNumber: true,
            width: '100%', 
            height: '100%',
            title: 'Confirmaciones por Sede',
        };

      var table = new google.visualization.Table(document.getElementById("table_confirmaciones_sedes"));
      table.draw(data, options);
    }  
      
    function drawChartConfirmacionesSedes() {
      var jsonData = $.ajax({
          url: '<?php echo base_url("dashboard/get_confirmaciones_sedes" );?>',
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      var options = {
            isStacked: true,
            height: 500,
            legend: {position: 'top', maxLines: 3},
            vAxis: {minValue: 0},
            bar: { groupWidth: '90%' },
            title: 'Confirmaciones por Sede',
        };

      var chart = new google.visualization.ColumnChart(document.getElementById("chart_confirmaciones_sedes"));
      chart.draw(data, options);
    }

    function drawChartConfirmadosHorarioFecha() {
      var jsonData = $.ajax({
          url: '<?php echo base_url("dashboard/get_confirmados_horario_fecha" );?>',
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      var options = {
            height: 300,
            legend: {position: 'top', maxLines: 4},
            vAxis: {minValue: 1},
            bar: { groupWidth: '90%' },
            title: 'Confirmaciones por Fecha y Horario',
            colors: ['#1C9E77', '#D95F02', '#7570B3', '#C0C0C0']
        };

      var chart = new google.visualization.ColumnChart(document.getElementById("chart_confirmaciones_fecha"));
      chart.draw(data, options);
    }

    function drawChartConfirmadosFFVV() {

        var jsonData = $.ajax({
          url: '<?php echo base_url("dashboard/get_confirmados_ffvv" );?>',
          dataType: "json",
          async: false
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        // Set options for ConfirmacionesFFVV's pie chart.
        var options = {
            title:'Confirmados por FFVV',
            width:400,
            height:300,
            colors: ['#DC3912', '#3366CC', '#FF9900']
        };

        // Instantiate and draw the chart for Anthony's pizza.
        var chart = new google.visualization.PieChart(document.getElementById('chart_confirmaciones_ffvv'));
        chart.draw(data, options);
      }

    </script>