<div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 login-box">
          <div class="panel panel-default">
            <div class="panel-intro text-center">
              <h2 class="logo-title"> 
                <!-- Original Logo will be placed here  --> 
                <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> Reserva<span>Heliópolis </span> 
              </h2>
            </div>
            <div class="panel-body">
                <?php 
                if($errormsg) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Error!</strong> <?=  $errormsg; ?>
                </div>
                <?php } ?>
                <?php   
                    $attributes = array( 'id' => 'floginUsuario', 'role'=>'form');
                    echo form_open('Login/checklogin',$attributes); 
                 ?>  
                    <div class="form-group">
                      <label for="user" class="control-label">Usuario:</label>
                      <div class="input-icon"> <i class="icon-user fa"></i>
                        <input id="user" name="user" type="text"  placeholder="Usuario" class="form-control" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="password"  class="control-label">Contraseña:</label>
                      <div class="input-icon"> <i class="icon-lock fa"></i>
                          <input type="password"  class="form-control" placeholder="Password" name="password" id="password" required >
                      </div>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block"/>  
                    </div>
                <?php form_close()?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

