/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    $("#pais").change(function(){
        
        var paisID = $("#pais").val();
        
        $.post( 
             baseurl+"ubigeo/buscarDepartamentos",
             {paisId: paisID},
             function(data){
                var departamento = jQuery.parseJSON(data);
                $.each(departamento, function(i, departamento){
                    $('#departamento').append("<option value='"+departamento.idDepartamento+"'>"+departamento.nombreDepartamento+"</option>");
                });
            }
        );    
    });
    
    $("#departamento").change(function(){
        
        var departamentoId = $("#departamento").val();
        
        $('#provincia option').remove();
        $('#provincia').append("<option value='' selected >Seleccione opción</option>");
        
        $.post( 
             baseurl+"ubigeo/buscarProvincias",
             {departamentoId: departamentoId},
             function(data){
                var provincia = jQuery.parseJSON(data);
                $.each(provincia, function(i, provincia){
                    $('#provincia').append("<option value='"+provincia.idProvincia+"'>"+provincia.nombreProvincia+"</option>");
                });
            }
        );
    });
    
    $("#provincia").change(function(){
        
        var provinciaId = $("#provincia").val();
        
        $('#distrito option').remove();
        $('#distrito').append("<option value='' selected >Seleccione opción</option>");
        
        $.post( 
             baseurl+"ubigeo/buscarDistritos",
             {provinciaId: provinciaId},
             function(data){
                var distrito = jQuery.parseJSON(data);
                $.each(distrito, function(i, distrito){
                    $('#distrito').append("<option value='"+distrito.idDistrito+"'>"+distrito.nombreDistrito+"</option>");
                });
            }
        );
    });
    
});

